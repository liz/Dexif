﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dexif_Installer {
  class Helper {
    // https://stackoverflow.com/a/3856090/4708786
    public static bool ExistsOnPath(string fileName) {
      return GetFullPath(fileName) != null;
    }

    public static string GetFullPath(string fileName) {
      if (File.Exists(fileName)) { 
        return Path.GetFullPath(fileName);
      }
      var values = Environment.GetEnvironmentVariable("PATH");
      foreach (var path in values.Split(Path.PathSeparator)) {
        var fullPath = Path.Combine(path, fileName);
        if (File.Exists(fullPath)) {
          return fullPath;
        }
      }
      return null;
    }
  }
}
