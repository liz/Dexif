﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


/* TODO:
 *  - Ensure Chocolatey
 *  - Ensure AutoHotKey (Questionable Requirement)
 *  - Ensure Exiftool
 *  - Install Exec
 *  - Install Reg Keys
 *  - Add to "Installed Programs"
 *  - Add uninstall functionality
 *  
 * (Very Very Future TODO)
 *  - Recursion Support for Directories
 *  - GUI
 *  - Configurable Suffix / Etc. Opts
*/

namespace Dexif_Installer {
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window {
    public MainWindow() {
      InitializeComponent();
    }

    public void WindowLoaded(object sender, RoutedEventArgs e) {
      EnsureChocolatey();
      EnsureExiftool();
    }

    private void EnsureChocolatey() {
      String ChocolateyExecutable = "choco.exe";
      String DefaultInstallPath = "C:\\ProgramData\\chocolatey\\bin\\";
      if (Helper.ExistsOnPath(DefaultInstallPath + ChocolateyExecutable)) {
        Status_Chocolatey.Icon = FontAwesome.WPF.FontAwesomeIcon.CheckCircleOutline;
        Locate_Chocolatey.Content = "Change";
        Status_Chocolatey.ToolTip = DefaultInstallPath + ChocolateyExecutable;
      } else if (Helper.ExistsOnPath(ChocolateyExecutable)) {
        Status_Chocolatey.Icon = FontAwesome.WPF.FontAwesomeIcon.CheckCircleOutline;
        Locate_Chocolatey.Content = "Change";
        Status_Chocolatey.ToolTip = Helper.GetFullPath(ChocolateyExecutable);
      } else {
        Status_Chocolatey.Icon = FontAwesome.WPF.FontAwesomeIcon.TimesCircleOutline;
        Status_Chocolatey.ToolTip = "Chocolatey not found.";
      }
      return;
    }
    private void EnsureExiftool() {
      String ExiftoolExecutable = "exiftool.exe";
      String DefaultInstallPath = " C:\\ProgramData\\chocolatey\\bin\\";
      if (Helper.ExistsOnPath(DefaultInstallPath + ExiftoolExecutable)) {
        Status_Exiftool.Icon = FontAwesome.WPF.FontAwesomeIcon.CheckCircleOutline;
        Locate_Exiftool.Content = "Change";
        Status_Exiftool.ToolTip = DefaultInstallPath + ExiftoolExecutable;
      } else if (Helper.ExistsOnPath(ExiftoolExecutable)) {
        Status_Exiftool.Icon = FontAwesome.WPF.FontAwesomeIcon.CheckCircleOutline;
        Locate_Exiftool.Content = "Change";
        Status_Exiftool.ToolTip = Helper.GetFullPath(ExiftoolExecutable);
      } else {
        Status_Exiftool.Icon = FontAwesome.WPF.FontAwesomeIcon.TimesCircleOutline;
        Status_Exiftool.ToolTip = "Exiftool not found.";
      }
      return;
    }
  }
}
